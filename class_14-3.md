# Cours d'intégration déploiment du 14 mars.

## SOMMAIRE
### [Package security](#package-manager-cli-security)
### [Trivy](#trivy)
### [GitLeaks](#gitleaks)
### [TODO](#a-faire)


## Package manager cli security

-  npm audit
    - analyse du package JSON + LOCK (qu'il faut mettre à jour)
        - SEMVER package.json -> MAJOR.MINOR.PATCH
            - ^A.B.C -> A.X.X (met à jour MINOR et PATCH)
            - ~A.B.C -> A.B.X (met à jour que les PATCH)
        - En cas de `npm update`
            - Comparaison entre le souhaité (package json) et le reel (package.lock)
    - affiche les CVE
        - low
        - mediums
        - high
        - critical

        (Chose intéressante : afficher les CVE -> que les HIGH, CRITICAL)
    
## Trivy

- Tool qui permet:
    - faire un point sur les vulnérabilité des images docker
    - voir les maj proposé pour les images docker
    - check config Dockerfile pour les bast practicies
        - image taguée
        - utilisation d'un user -> proscrire le root
        - healthcheck/prob



- Semblable à npm audit ?
    - oui -> analyser les modules NPM dispo dans le package.json
    - Non -> car il va analyser la base de l'image

- Use Case:
    - Display HIGH/CRIT, code output 0, fixed + unfixed
    - Display HIGH.CRIT, code output 1, fixed

WAF = Web Application Firework

## GitLeaks

- Besoin d'une info sensible dans le code ? Pour 
    - connexion DB
    - connexion comptes de services
    - test e2e compte diverses
    - connexion third parts
    - API tierces
    - etc ..

- <span style="color:red">/!\ JAMAIS D'INFO SENSIBLE EN CLAIRE DANS LE CODE /!\ </span>


## A faire : 

### Docker

Build & run docker image en local

Faire le job "check docker" avec kaniko

Push le tout sur gitlab 