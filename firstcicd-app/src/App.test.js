import React from 'react';
import Button from './component/Bouton';
import { expect, test } from '@jest/globals';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
	render(<App />);
	const linkElement = screen.getByText(/learn react/i);
	expect(linkElement).toBeInTheDocument();
});

test('renders button with correct text', () => {
	const { getByText } = render(<Button />);
	const buttonElement = getByText(/click me/i);
	expect(buttonElement).toBeInTheDocument();
	expect(buttonElement).toBeVisible();
});