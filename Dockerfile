# run with "docker run . -it imageName  /bin/bash"
# run port "src:dest" -> ""

# Stage 1: Build app with dependencies
FROM node:20-alpine3.19 AS build
WORKDIR /app
COPY firstcicd-app/package*.json ./
COPY firstcicd-app/src/ ./src
COPY firstcicd-app/public/ ./public
RUN npm ci
RUN npm run build

# Stage 2: Create app
FROM node:20-alpine3.19
WORKDIR /app
COPY --from=build /app/build ./build

RUN npm install -g http-server

EXPOSE 3000

CMD ["http-server", "./build", "-p", "3000"]